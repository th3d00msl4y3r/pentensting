# LAN Turtle

```
IP Address : 172.16.84.1
Port: 22
Username: root
Password: sh3llz
```

## Configuration LAN Turtle

`ssh root@172.16.84.1`

## Update LAN Turtle Manually

`scp upgrade-6.2.bin root@172.16.84.1:/tmp`
`sysupgrade -n /tmp/upgrade-6.2.bin`

# References
https://docs.hak5.org/lan-turtle/ \
https://docs.hak5.org/lan-turtle/faq-troubleshooting/manual-upgrade




