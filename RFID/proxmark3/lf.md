# Low frequency

## Clonar tarjeta HID a T5577

Identificar tipo de tarjeta.

`lf search`

```
[usb] pm3 --> lf search

[=] NOTE: some demods output possible binary
[=] if it finds something that looks like a tag
[=] False Positives ARE possible
[=] 
[=] Checking for known tags...
[=] 
[+] [H10301  ] HID H10301 26-bit                FC: 17  CN: 33845  parity ( ok )
[+] [ind26   ] Indala 26-bit                    FC: 280  CN: 1077  parity ( ok )
[=] found 2 matching formats
[+] DemodBuffer:
[+] 1D5559555569595A55956999

[=] raw: 00000000000000200623086a

[+] Valid HID Prox ID found!

[=] Couldn't identify a chipset
```

Leer datos de la tarjeta HID en especifico el ID. 

`lf hid reader`

```
[usb] pm3 --> lf hid reader
[+] [H10301  ] HID H10301 26-bit                FC: 17  CN: 33845  parity ( ok )
[+] [ind26   ] Indala 26-bit                    FC: 280  CN: 1077  parity ( ok )
[=] found 2 matching formats
[+] DemodBuffer:
[+] 1D5559555569595A55956999

[=] raw: 00000000000000200623086a
```

Tarjeta T5577 vacia.

```
[usb] pm3 --> lf search

[=] NOTE: some demods output possible binary
[=] if it finds something that looks like a tag
[=] False Positives ARE possible
[=] 
[=] Checking for known tags...
[=] 
[-] ⛔ No known 125/134 kHz tags found!
[+] Chipset detection: T55xx
[?] Hint: try `lf t55xx` commands
```

Clonar tarjeta HID a T55xx.

`lf hid clone -r 200623086a`

```
[usb] pm3 --> lf hid clone -r 200623086a
[=] Preparing to clone HID tag using raw 200623086a
[=] Done
[?] Hint: try `lf hid reader` to verify
```

Verificar que la tarjeta se clono correctamente.

`lf hid reader`

Tarjeta T5577.

```
[usb] pm3 --> lf hid reader
[+] [H10301  ] HID H10301 26-bit                FC: 17  CN: 33845  parity ( ok )
[+] [ind26   ] Indala 26-bit                    FC: 280  CN: 1077  parity ( ok )
[=] found 2 matching formats
[+] DemodBuffer:
[+] 1D5559555569595A55956999

[=] raw: 00000000000000200623086a
```

Tarjeta Original.

```
[usb] pm3 --> lf hid reader
[+] [H10301  ] HID H10301 26-bit                FC: 17  CN: 33845  parity ( ok )
[+] [ind26   ] Indala 26-bit                    FC: 280  CN: 1077  parity ( ok )
[=] found 2 matching formats
[+] DemodBuffer:
[+] 1D5559555569595A55956999

[=] raw: 00000000000000200623086a
```

## Simular tarjeta con Proxmark3

`lf hid sim -r 200623086a`

```
[usb] pm3 --> lf hid sim -r 200623086a
[=] Simulating HID tag using raw 200623086a

[=] Press <Enter> or pm3-button to abort simulation
[#] FSK simulating with rf/50, fc high 10, fc low 8, STT 0, n 4800
```

## Clonar tarjetas en modo Standalone

### Flashing Proxmark3

Renombrar archivo Makefile.

`mv Makefile.platform.sample Makefile.platform`

Descomentar linea STANDALONE y agregarle un valor.

`nano Makefile.platform`

```
# If you want to use it, copy this file as Makefile.platform and adjust it to your needs
# Run 'make PLATFORM=' to get an exhaustive list of possible parameters for this file.

PLATFORM=PM3RDV4
#PLATFORM=PM3GENERIC
# If you want more than one PLATFORM_EXTRAS option, separate them by spaces:
#PLATFORM_EXTRAS=BTADDON
STANDALONE=LF_SAMYRUN

# To accelerate repetitive compilations:
# Install package "ccache" -> Debian/Ubuntu: /usr/lib/ccache, Fedora/CentOS/RHEL: /usr/lib64/ccache
# And uncomment the following line
#export PATH := /usr/lib64/ccache:/usr/lib/ccache:${PATH}

# To install with sudo:
#INSTALLSUDO=sudo
```

Compilar los binarios.

`make clean && make -j`

Flashear proxmark.

`./pm3-flash-all`

```
┌──(root㉿mcfly)-[~/pentest/RFID/proxmark3]
└─# ./pm3-flash-all       
[=] Session log /root/.proxmark3/logs/log_20220601.txt
[+] loaded from JSON file /root/.proxmark3/preferences.json
[+] About to use the following files:
[+]    /root/pentest/RFID/proxmark3/client/../bootrom/obj/bootrom.elf
[+]    /root/pentest/RFID/proxmark3/client/../armsrc/obj/fullimage.elf
[+] Loading ELF file /root/pentest/RFID/proxmark3/client/../bootrom/obj/bootrom.elf
[+] ELF file version Iceman/master/v4.14831-607-gf1e228e73-dirty-unclean 2022-06-01 14:55:41 1169a2b5a

[+] Loading ELF file /root/pentest/RFID/proxmark3/client/../armsrc/obj/fullimage.elf
[+] ELF file version Iceman/master/v4.14831-607-gf1e228e73-dirty-unclean 2022-06-01 14:55:47 1169a2b5a

[+] Waiting for Proxmark3 to appear on /dev/ttyACM0
 🕑  59 found
[+] Entering bootloader...
[+] (Press and release the button only to abort)
[+] Waiting for Proxmark3 to appear on /dev/ttyACM0
 🕑  49 found
[=] Available memory on this board: 512K bytes

[=] Permitted flash range: 0x00100000-0x00180000
[+] Loading usable ELF segments:
[+]    0: V 0x00100000 P 0x00100000 (0x00000200->0x00000200) [R X] @0x94
[+]    1: V 0x00200000 P 0x00100200 (0x00000d1c->0x00000d1c) [R X] @0x298

[+] Loading usable ELF segments:
[+]    1: V 0x00102000 P 0x00102000 (0x0004ea44->0x0004ea44) [R X] @0xb8
[+]    2: V 0x00200000 P 0x00150a44 (0x00001b08->0x00001b08) [R X] @0x4eb00
[=] Note: Extending previous segment from 0x4ea44 to 0x5054c bytes

[+] Flashing...
[+] Writing segments for file: /root/pentest/RFID/proxmark3/client/../bootrom/obj/bootrom.elf
[+]  0x00100000..0x001001ff [0x200 / 1 blocks]
. ok
[+]  0x00100200..0x00100f1b [0xd1c / 7 blocks]
....... ok

[+] Writing segments for file: /root/pentest/RFID/proxmark3/client/../armsrc/obj/fullimage.elf
[+]  0x00102000..0x0015254b [0x5054c / 643 blocks]
...................................................................
        @@@  @@@@@@@ @@@@@@@@ @@@@@@@@@@   @@@@@@  @@@  @@@
        @@! !@@      @@!      @@! @@! @@! @@!  @@@ @@!@!@@@
        !!@ !@!      @!!!:!   @!! !!@ @!@ @!@!@!@! @!@@!!@!
        !!: :!!      !!:      !!:     !!: !!:  !!! !!:  !!!
        :    :: :: : : :: :::  :      :    :   : : ::    : 
        .    .. .. . . .. ...  .      .    .   . . ..    . 
...................................................................
...................................................................
...................................................................... ok

[+] All done

[=] Have a nice day!
```

### Procedimiento de clonado

- Se puede desconectar la proxmark de la pc y conectarla a una bateria externa.
- Presionar el boton blanco por 2 segundos y se iluminaran de izquierda a derecha los leds, posteriormente se apagaran.
- Matener presionado el boton blanco hasta ver la letra **A** y **D** prendidas.
- Pasar la tarjeta que se quiere clonar. Una vez se hizo esto se apagaran los leds rojos.
- Mantener presionado el boton blanco hasta que se ilumine la letra **C**.
- Ahora puede utilizar la tarjeta donde se necesite.

# Referencias
https://scund00r.com/all/rfid/2018/06/05/proxmark-cheatsheet.html \
https://github.com/RfidResearchGroup/proxmark3/wiki/Standalone-mode

