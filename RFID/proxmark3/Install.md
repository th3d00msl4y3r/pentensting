# Installation

## Install dependencies
```
sudo apt update
sudo apt install --no-install-recommends git ca-certificates build-essential pkg-config 
libreadline-dev gcc-arm-none-eabi libnewlib-dev qtbase5-dev libbz2-dev libbluetooth-dev libpython3-dev libssl-dev
```

## Remove modemmanager
```
apt remove modemmanager
```

## Connect proxmark3 
```
dmesg | grep -i usb
```

```
[ 2110.973550] usb 3-3: Product: proxmark3
[ 2110.973552] usb 3-3: Manufacturer: proxmark.org
[ 2110.973555] usb 3-3: SerialNumber: iceman
[ 2111.003841] cdc_acm 3-3:1.0: ttyACM0: USB ACM device
```

We can see **ttyACM0** device.

```
ls -la /dev/ttyACM0
```

## Download Repository and Compile
```
git clone https://github.com/RfidResearchGroup/proxmark3.git
cd proxmark3
make clean && make -j
```

## Flash the BOOTROM & FULLIMAGE

```
./pm3-flash-all
```

```
┌──(root㉿mcfly)-[~/pentest/RFID/proxmark3]
└─# ./pm3-flash-all 
[=] Session log /root/.proxmark3/logs/log_20220507.txt
[+] About to use the following files:
[+]    /root/pentest/RFID/proxmark3/client/../bootrom/obj/bootrom.elf
[+]    /root/pentest/RFID/proxmark3/client/../armsrc/obj/fullimage.elf
[+] Loading ELF file /root/pentest/RFID/proxmark3/client/../bootrom/obj/bootrom.elf
[+] ELF file version Iceman/master/v4.14831-574-g9ad9afbb6 2022-05-07 11:41:54 621e5fef2

[+] Loading ELF file /root/pentest/RFID/proxmark3/client/../armsrc/obj/fullimage.elf
[+] ELF file version Iceman/master/v4.14831-574-g9ad9afbb6 2022-05-07 11:42:01 621e5fef2

[+] Waiting for Proxmark3 to appear on /dev/ttyACM0
 🕑  59 found
[+] Entering bootloader...
[+] (Press and release the button only to abort)
[+] Waiting for Proxmark3 to appear on /dev/ttyACM0
 🕑  49 found
[=] Available memory on this board: 512K bytes

[=] Permitted flash range: 0x00100000-0x00180000
[+] Loading usable ELF segments:
[+]    0: V 0x00100000 P 0x00100000 (0x00000200->0x00000200) [R X] @0x94
[+]    1: V 0x00200000 P 0x00100200 (0x00000d1c->0x00000d1c) [R X] @0x298

[+] Loading usable ELF segments:
[+]    1: V 0x00102000 P 0x00102000 (0x0004ea0c->0x0004ea0c) [R X] @0xb8
[+]    2: V 0x00200000 P 0x00150a0c (0x00001b0a->0x00001b0a) [R X] @0x4eac8
[=] Note: Extending previous segment from 0x4ea0c to 0x50516 bytes

[+] Flashing...
[+] Writing segments for file: /root/pentest/RFID/proxmark3/client/../bootrom/obj/bootrom.elf
[+]  0x00100000..0x001001ff [0x200 / 1 blocks]
. ok
[+]  0x00100200..0x00100f1b [0xd1c / 7 blocks]
....... ok

[+] Writing segments for file: /root/pentest/RFID/proxmark3/client/../armsrc/obj/fullimage.elf
[+]  0x00102000..0x00152515 [0x50516 / 643 blocks]
...................................................................
        @@@  @@@@@@@ @@@@@@@@ @@@@@@@@@@   @@@@@@  @@@  @@@
        @@! !@@      @@!      @@! @@! @@! @@!  @@@ @@!@!@@@
        !!@ !@!      @!!!:!   @!! !!@ @!@ @!@!@!@! @!@@!!@!
        !!: :!!      !!:      !!:     !!: !!:  !!! !!:  !!!
        :    :: :: : : :: :::  :      :    :   : : ::    : 
        .    .. .. . . .. ...  .      .    .   . . ..    . 
...................................................................
...................................................................
...................................................................... ok

[+] All done

[=] Have a nice day!
```

## Connect to proxmark3
```
./pm3
```

```
┌──(root㉿mcfly)-[~/pentest/RFID/proxmark3]
└─# ./pm3          
[=] Session log /root/.proxmark3/logs/log_20220507.txt
[=] Using UART port /dev/ttyACM0
[=] Communicating with PM3 over USB-CDC


  8888888b.  888b     d888  .d8888b.   
  888   Y88b 8888b   d8888 d88P  Y88b  
  888    888 88888b.d88888      .d88P  
  888   d88P 888Y88888P888     8888"  
  8888888P"  888 Y888P 888      "Y8b.  
  888        888  Y8P  888 888    888  
  888        888   "   888 Y88b  d88P 
  888        888       888  "Y8888P"    [ ❄ ]


[=] Creating initial preferences file
[=] Saving preferences...
[+] saved to json file /root/.proxmark3/preferences.json
  [ Proxmark3 RFID instrument ]

    MCU....... AT91SAM7S512 Rev A
    Memory.... 512 Kb ( 64% used )

    Client.... Iceman/master/v4.14831-574-g9ad9afbb6 2022-05-07 11:42:01
    Bootrom... Iceman/master/v4.14831-574-g9ad9afbb6 2022-05-07 11:41:54 
    OS........ Iceman/master/v4.14831-574-g9ad9afbb6 2022-05-07 11:42:01 
    Target.... device / fw mismatch


[!!] 🚨 No history will be recorded
[usb] pm3 -->
```

## Upgrade SIM module
```
[usb] pm3 --> smart upgrade -f ./client/resources/sim011.bin
[=] -------------------------------------------------------------------
[!] ⚠  WARNING - sim module firmware upgrade
[!] ⚠  A dangerous command, do wrong and you could brick the sim module
[=] -------------------------------------------------------------------

[=] firmware file       ./client/resources/sim011.bin
[=] Checking integrity  ./client/resources/sim011.sha512.txt
[+] loaded 733 bytes from binary file ./client/resources/sim011.bin
[+] loaded 141 bytes from binary file ./client/resources/sim011.sha512.txt
[=] Don't turn off your PM3!
[+] Sim module firmware uploading to PM3...
 🕐 733 bytes sent
[+] Sim module firmware updating...
[#] FW 0000
[#] FW 0080
[#] FW 0100
[#] FW 0180
[#] FW 0200
[#] FW 0280
[+] Sim module firmware upgrade successful
[?] run `hw status` to validate the fw version
```

```
[usb] pm3 --> hw status
[#] Memory                                                                                                                                       
[#]   BigBuf_size............. 40912                                                                                                             
[#]   Available memory........ 40912                                                                                                             
[#] Tracing                                                                                                                                      
[#]   tracing ................ 1                                                                                                                 
[#]   traceLen ............... 0                                                                                                                 
[#] Current FPGA image                                                                                                                           
[#]   mode.................... HF image 2s30vq100 2022-03-23 17:21:16                                                                            
[#] Flash memory                                                                                                                                 
[#]   Baudrate................ 24 MHz                                                                                                            
[#]   Init.................... OK                                                                                                                
[#]   Memory size............. 2 mbits / 256 kb                                                                                                  
[#]   Unique ID............... 0xD5697C3097285423                                                                                                
[#] Smart card module (ISO 7816)                                                                                                                 
[#]   version................. v3.11                                                                                                             
[#] LF Sampling config                                                                                                                           
[#]   [q] divisor............. 95 ( 125.00 kHz )                                                                                                 
[#]   [b] bits per sample..... 8
[#]   [d] decimation.......... 1
[#]   [a] averaging........... yes
[#]   [t] trigger threshold... 0
[#]   [s] samples to skip..... 0
```

# References
https://github.com/RfidResearchGroup/proxmark3/blob/master/doc/md/Installation_Instructions/Linux-Installation-Instructions.md \
https://github.com/RfidResearchGroup/proxmark3/blob/master/doc/md/Use_of_Proxmark/0_Compilation-Instructions.md