# Traffic Analysis

## Tshark

**Supported network interfaces**
```
tshark -D
```

**Sniff interface**
```
tshark -i eth0
```

**Read pcap files**
```
tshark -r traffic.pcap
tshark -r traffic.pcap | wc -l
tshark -r traffic.pcap -c 100
tshark -r traffic.pcap -z io,phs -q
```

**Filtering HTTP traffic**
```
tshark -r HTTP_traffic.pcap -Y 'http'
tshark -r HTTP_traffic.pcap-Y "ip.src==192.168.252.128 && ip.dst==52.32.74.91"
tshark -r HTTP_traffic.pcap-Y "http.request.method==GET"
tshark -r HTTP_traffic.pcap-Y "http.request.method==GET" -Tfields -e frame.time -e ip.src -e http.request.full_uri
tshark -r HTTP_traffic.pcap-Y "http contains password"
tshark -r HTTP_traffic.pcap-Y "http.request.method==GET && http.host==www.nytimes.com" -Tfields -e ip.dst
tshark -r HTTP_traffic.pcap -Y "ip contains amazon.in && ip.src==192.168.252.128" -Tfields -e ip.src -e http.cookie
tshark -r HTTP_traffic.pcap -Y "ip.src==192.168.252.128 && http" -Tfields -e http.user_agent
```

**Filtering HTTPS traffic**
```

```