## Basic Scanning Techniques

### Scan Single Target
`nmap 192.168.10.1`

### Scan Multiple Targets
`nmap 192.168.10.1 192.168.10.100 192.168.10.101`\
`nmap 192.168.10.1,100,101`

### Scan Range IP Addresses
`nmap 192.168.10.1-100`\
`nmap 192.168.1-100.*`\
`nmap 192.168.100.*`

### Scan Entire Subnet
`nmap 192.168.10.1/24`

### Scan List of Targets
`nmap -iL list.txt`

##### list.txt
```
192.168.10.1
192.168.10.100
192.168.10.101
```

### Scan Random Targets
`nmap -iR [number of targets]`\
`nmap -iR 3`

### Exclude Targets Scan
`nmap 192.168.10.0/24 --exclude 192.168.10.100`\
`nmap 192.168.10.0/24 --exclude 192.168.10.100-105`

### Exclude Targets Using a List
`nmap 192.168.10.0/24 --excludefile list.txt`

##### list.txt
```
192.168.10.1
192.168.10.12
192.168.10.44
```

### Aggressive Scan
`nmap -A 10.10.1.51`

### Scan IPv6 Target
`nmap -6 fe80::29aa:9db9:4164:d80e`

## Discovery Options

`nmap -Pn 10.10.5.11`

### Ping Only Scan
`nmap -sP 192.168.10.2/24`



